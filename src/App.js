//import React, { Component } from "react";
import {useState} from "react";
import Listing from "./components/listing_page";
import Add from "./components/add_contact";
import "./App.css";
import { Switch, Route } from "react-router-dom";
import Editing from "./components/Editing";

function App()
{

  const [data, setData]= useState( [
    {
      first_name: "Akanksha",
      last_name: "Bhardwaj",
      gender: "Female",
      dob: "2000-01-01",
      phone_number: "9650536742",
      locations: [{label:"Mumbai"},{ label:"Bangalore"}],
      image: "https://wallpapercave.com/wp/wp7826056.jpg",
      id: 10,
    },
    {
      first_name: "Ashutosh",
      last_name: "Sharma",
      gender: "Male",
      dob: "2000-02-02",
      phone_number: "9999998888",
      locations: [{label:"Goa"},{ label:"Bangalore"}],
      image: "http://essenziale-hd.com/wp-content/uploads/2017/10/patio-1-1.jpg",
      id: 11,
    }
  ],
);


const handleAddSubmit = (user) => {
  console.log("add submit triggered")
    console.log(user)

    setData([...data, user])
 
};



const handleEditing=(modified_user)=>{

 
  let arr =[...data];

  for(let i=0;i<data.length;i++)
  {
    if(data[i].id===modified_user.id)
    arr[i]=modified_user;
  }

 setData(arr);

}




const handleDelete=(id)=>{

console.log("delete event triggered",id)

let arr = data.filter((obj)=>
{
return obj.id !==id
})

setData(arr);




}












  return(

    <Switch>
    <Route
      exact
      path="/"
      render={() => (
        <Listing data={data}  handleDelete={handleDelete} />
      )}
    />
    <Route
      exact
      path="/add_user"
      render={() => <Add add_submit={handleAddSubmit} />}
    />

    <Route exact path="/editing-user/:handle"
     render={(props) => <Editing {...props}  handleEditing={handleEditing}/>}
     
    
    
    />
  </Switch>




  );
}


export default App;









//---------------------------------------------------------------------------------------------------------------


/*
class App extends Component {
  state = {
    data: [
      {
        first_name: "Akanksha",
        last_name: "Bhardwaj",
        gender: "Female",
        dob: "2000-01-01",
        phone_number: "9650536742",
        locations: [{label:"Mumbai"},{ label:"Bangalore"}],
        image: "https://wallpapercave.com/wp/wp7826056.jpg",
        id: 10,
      },
      {
        first_name: "Ashutosh",
        last_name: "Sharma",
        gender: "Male",
        dob: "2000-02-02",
        phone_number: "9999998888",
        locations: [{label:"Goa"},{ label:"Bangalore"}],
        image: "http://essenziale-hd.com/wp-content/uploads/2017/10/patio-1-1.jpg",
        id: 11,
      }
    ],

   
  };

  handleAddSubmit = (user) => {
    //console.log(user)
    this.setState({ data: [...this.state.data, user] }, () => {
      console.log(this.state);
    });
  };

 

  handleEditing=(modified_user)=>{

   
    let arr =[...this.state.data];

    for(let i=0;i<this.state.data.length;i++)
    {
      if(this.state.data[i].id===modified_user.id)
      arr[i]=modified_user;
    }

    this.setState({data: arr, show:false},()=>console.log("modified state", this.state.data))


  }




  handleDelete=(id)=>{

console.log("delete event triggered",id)

let arr = this.state.data.filter((obj)=>
{
return obj.id !==id
})

this.setState({data: arr})




}

  render() {
    return (
      <Switch>
        <Route
          exact
          path="/"
          render={() => (
            <Listing state={this.state}  handleSaveChanges={this.handleSaveChanges} handleDelete={this.handleDelete} />
          )}
        />
        <Route
          exact
          path="/add_user"
          render={() => <Add add_submit={this.handleAddSubmit} />}
        />
        <Route exact path="/editing-user/:handle"
         render={(props) => <Editing {...props}  handleEditing={this.handleEditing}/>}
         
        
        
        />
      </Switch>
    );
  }
}

export default App;


*/