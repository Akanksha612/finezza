import React from "react";
import { Link } from "react-router-dom";
import "../App.css";



function Listing(props)
{

  console.log(props)

return (

<>
        <div className="main">
          <Link to="/add_user" className="border" style={{ width: 1500 }}>
            <button className="btn btn-info d-flex add-button">
              Add contact
            </button>
          </Link>

          {props.data.map((user) => {
            return (
              <div>

                <div
                  key={user.id}
                  className=" d-flex justify-content-center"
                  
                >
                  <div
                    className=" box d-flex m-2 p-3 border"
                    style={{ height: 200, width: 580 }}
                  >
                    <div className="d-flex align-items-center">
                      <img
                        src={user.image}
                        alt="upload pic"
                        className="rounded-circle"
                        style={{ maxHeight: 170, maxWidth: 180 }}
                      />
                      <section className="info">
                        <h6>
                          Name:{" "}
                          <span className="fs-6 fw-light">
                            {user.first_name} {user.last_name}
                          </span>
                        </h6>

                        <h6>
                          Phone Number:{" "}
                          <span className="fs-6 fw-light">
                            {user.phone_number}
                          </span>
                        </h6>
                        <h6>
                          DOB: <span className="fs-6 fw-light">{user.dob}</span>
                        </h6>

                        <h6>
                          Gender:{" "}
                          <span className="fs-6 fw-light">{user.gender}</span>
                        </h6>
                        <h6>
                          Preffered Locations:{" "}
                          <span className="fs-6 fw-light">
                            {user.locations.map((loc) => loc.label + " ")}
                          </span>
                        </h6>
                      </section>

                      <div className="d-flex flex-column button-container ">
                        <button
                          className="btn btn-danger button "
                          onClick={() => {
                            props.handleDelete(user.id);
                          }}
                        >
                          Delete
                        </button>

                        <button
                          className=" btn btn-light button"
                        >

                        
                          <Link to={{ pathname:`/editing-user/${user.id}`,
                          hello:{lovely:{user}}}} 
                          
                          className="text-dark text-decoration-none"   
                          
                          >

                           
                       
                          Edit
                          </Link>
                          </button>
                      </div>
                    </div>
                  </div>
                </div>

              
               </div>
            ); //map end
          })}
        </div>
      </>
    );






}


export default Listing;




















//---------------------------------------------------------------------------------------------------------------------------


/*
class Listing extends Component {
  options = [
    { value: "bangalore", label: "Bangalore" },
    { value: "mumbai", label: "Mumbai" },
    { value: "goa", label: "Goa" },
  ];

  

   render() {
    console.log("props", this.props);

    return (
      <>
        <div className="main">
          <Link to="/add_user" className="border" style={{ width: 1500 }}>
            <button className="btn btn-info d-flex add-button">
              Add contact
            </button>
          </Link>

          {this.props.state.data.map((user) => {
            return (
              <div>

                <div
                  key={user.id}
                  className=" d-flex justify-content-center"
                  
                >
                  <div
                    className=" box d-flex m-2 p-3 border"
                    style={{ height: 200, width: 580 }}
                  >
                    <div className="d-flex align-items-center">
                      <img
                        src={user.image}
                        alt="upload pic"
                        className="rounded-circle"
                        style={{ maxHeight: 170, maxWidth: 180 }}
                      />
                      <section className="info">
                        <h6>
                          Name:{" "}
                          <span className="fs-6 fw-light">
                            {user.first_name} {user.last_name}
                          </span>
                        </h6>

                        <h6>
                          Phone Number:{" "}
                          <span className="fs-6 fw-light">
                            {user.phone_number}
                          </span>
                        </h6>
                        <h6>
                          DOB: <span className="fs-6 fw-light">{user.dob}</span>
                        </h6>

                        <h6>
                          Gender:{" "}
                          <span className="fs-6 fw-light">{user.gender}</span>
                        </h6>
                        <h6>
                          Preffered Locations:{" "}
                          <span className="fs-6 fw-light">
                            {user.locations.map((loc) => loc.label + " ")}
                          </span>
                        </h6>
                      </section>

                      <div className="d-flex flex-column button-container ">
                        <button
                          className="btn btn-danger button "
                          onClick={() => {
                            this.props.handleDelete(user.id);
                          }}
                        >
                          Delete
                        </button>

                        <button
                          className=" btn btn-light button"
                        >

                        
                          <Link to={{ pathname:`/editing-user/${user.id}`,
                          hello:{lovely:{user}}}} 
                          
                          className="text-dark text-decoration-none"   
                          
                          >

                           
                       
                          Edit
                          </Link>
                          </button>
                      </div>
                    </div>
                  </div>
                </div>

              
               </div>
            ); //map end
          })}
        </div>
      </>
    ); //render end
  }
}

export default Listing;



*/


















